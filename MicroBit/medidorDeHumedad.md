## Traducción del tutorial del medidor de Humedad 

Fuente: [medidor de humedad de de Make code](https://makecode.microbit.org/projects/soil-moisture)

![](assets/soil-moisture.jpg)

### Materiales

- 1 micro:bit Con el pack de batería
- 2 clavos largos
- 2 pinzas cocodrilos con sus cables

### Actividades

#### Código

Van a codear el medidor de humedad usando un pote con tierra seca y uno con tierra mojada. Esto es para que puedan configurar el micro:bit sabiendo como son las condiciones de ambos escenarios, el seco y el humedo.

![](assets/nailsv3.jpg)

##### Paso 1: midiendo la humedad
El suelo y la tierra tienen algo de resistencia eléctrica que depende de la cantidad de agua que haya y de los nutrientes que tenga la tierra. Actua como una resistencia eléctrica variable en un circuito eléctrico. El agua no es conductiva, pero los nutrientes que tiene si. La combinación de agua y nutrientes del suelo hace que este tengoa algo de conductividad. Entonces, mientras más agua haya combinada con los nutrientes, menos conductividad eléctrica tendrá el suelo.

Para medir esto, leeremos el voltaje en el pin 0 usando la lectura analógica del pin 0 que nos devuelve un valor entre 0 (no hay corriente) y 1023 (máximo de corriente). El valor se grafica en la pantalla de la micro:bit usando plot bar graph (grafico de barras).

![](assets/CodigoHumedad1.png)

##### A experimentar

- Inserten los clavos en la tierra seca y conecten los cocodrilos de los clavos al Pin0 de la micro:bit y al pin GND. 
- Inserten los clavos en la tierra seca y conecten los cocodrilos de los clavos al Pin0 de la micro:bit y al pin GND.

##### Paso 2: medir el valor de los datos

En el programa anterior, solo tenemos una vaga idea de cual es valor de lo que medimos ¡Solo usamos una pantalla pequeñita para ver este valor! Vamos a agregar código que muestre el valor de lo que medimos cuando tocamos el botón A

Vamos a necesitar meternos en el bucle "para siempre" y también vamos a necesitar una variable para guardar lo que el sensor lee. En el ejemplo la variable se llama "valordevoltaje"

![](assets/CodigoHumedad2.png)

##### A experimentar

- Inserten los clavos en la tierra seca, presionen A y vean el valor, debería ser un valor cercano a 250 para la tierra seca.
- Inserten los clavos en la tierra mojada, presionen A y vean el valor, debería ser un valor cercano a 1000 para la tierra mojada.

ACUERDENSE QUE TIENEN QUE ESPERAR AL MENOS 10 SEGUNDOS ANTES DE CAMBIAR LOS CLAVOS DE LA TIERRA SECA A LA MOJADA
