<span style="font-size: 14px; background-color: #ffffff; color: #ff0000;">**Vamos a hacer una medidor de amor ¡Qué ternura!**</span>
Esta es una traducción del tutorial LOVE METER de [MakeCode](https://makecode.microbit.org/)
Podemos hacer un juego para que la Micro:Bit nos de un núimero cunado tocamos el **PIN 0**

![](https://pxt.azureedge.net/blob/c50634963c6fd99e527e99582979c4ba1b187ef1/static/mb/projects/love-meter/love-meter.gif)

Para construir nuestra máquina medidora del amor:

1.  1.  Agerguen un bloque "**Al presionar PIN 0**" para que nuestro programa arranque cunado se presiona el pin 0\. Pongan _P0_ en la lista de pines que pueden elegir en este bloque.
    2.  Usando los bloques **"mostrar número**" , que está en _básicos_ y "**escoger al azar de**" de la paleta _matemática_ armen un bloque que muestre un número al azar entre 0 y 100 cuando se presiona el pin 0

[Si están trabades, hagan click en este link](/MicroBit/assets/Anotaci%C3%B3n_2019-10-08_094236.png)

1.  Hagan click en el pin 0 del simulador para ver que números es el elegido
2.  Mostrar "**Medidor de Amor**" en la pantalla cuando se incia la Micro:Bit
3.  Hagan click en **Descargar** para transeferir su código  a la Micro:Bit. Una vez que lo transiferan, al presionar el pin **GND** con una mano y el **PIN0** con la otra mano se activará el código.