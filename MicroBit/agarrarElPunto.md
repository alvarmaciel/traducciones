Agarrá el punto es un juego de habilidad donde el jugador tiene que apretar el botón A exactamente cuando el punto está en el centro de la pantalla.
este tutorial muestra como usar la sección "juegos"
1. De la paleta **"Juego"**vamos a usar el bloque **"create sprite at x:2 y:2"**. un sprite es un unico pixel que se puede mover por la pantalla de leds. Tiene una posición x y una posición y junto con una dirección. Funciona como un objeto en Scratch.
2. crear una variable de nombre "punto" esta variable va a guardar la posción del sprite para saber en qué posición está
3. **Programar el siguiente algoritmo**
  - Al iniciar
  - Establecer punto para create sprite at x:2 y:2
  Así asignamos la variable punto al sprite y podemos saber por donde anda cuando se mueva

[Ayuda si no lo lograste](https://gitlab.com/alvarmaciel/traducciones/raw/master/MicroBit/assets/agarra1.png)
4. El sprite empieza en el centro apuntando a la derecha. Pongan un bloque "punto desplazar 1" dentro de un bloque "por siempre" para que se mueva hacia la derecha
  Fijense que no rebota en el borde ¿Qué bloque podrían poner para que rebote en el borde? Si ya les rebota en el borde ¿Cómo haría para que se mueva más lento?

[Ayuda si no salió](https://gitlab.com/alvarmaciel/traducciones/raw/master/MicroBit/assets/agarra2.png)
5. Ahora nos queda programar:
- Cuando el boton A esté presionado
  - si punto x = 2 entonces
    - agregar puntos a la puntuación actual 1
  - si no
    - fin del juego

**Ayuda**: Para armar el algoritmo de verificación, van a tener que construir un bloque con las paletas de "lógica" que tiene la opcion 0=0 y remplazar el primer 0 po punto x

[Ayuda si no salió](https://gitlab.com/alvarmaciel/traducciones/raw/master/MicroBit/assets/agarra3.png]