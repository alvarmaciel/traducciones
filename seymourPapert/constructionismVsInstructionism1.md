# Constructionism vs. Instructionism

In the 1980s Seymour Papert delivered the following speech by video to a conference of educators in Japan.


## Part 1: Teaching vs. Learning

[Real video file](http://www.papert.org/media/video/articles/const_inst/const_inst1.ram)

Seymour Papert: Hello. Greetings to Japanese educators. I'm sorry I'm not there with you, but fortunately we live in the century of technology, and I can use this magical image-making machine to send my picture, my voice, half way across the world. It's really wonderful.

What I was going to talk about if I had been there, is about how technology can change the way that children learn mathematics. I said how children can learn mathematics differently, not so much how we can teach mathematics differently. This is an important distinction.

All my work is focused on helping children learn, not on just teaching. Now I've coined a phrase for this: Constructionism and Instructionism are names for two approaches to educational innovation. Instructionism is the theory that says, "To get better education, we must improve instruction. And if we're going to use computers, we'll make the computers do the instruction." And that leads into the whole idea of computer-aided instruction.

Well, teaching is important, but learning is much more important. And Constructionism means "Giving children good things to do so that they can learn by doing much better than they could before." Now, I think that the new technologies are very, very rich in providing new things for children to do so that they can learn mathematics as part of something real.

I think part of the trouble with learning mathematics at school is that it's not like mathematics in the real world. In the real world, there are engineers, who use mathematics to make bridges or make machines. There are scientists, who use mathematics to make theories, to make explanations of how atoms work, and how the universe started. There are bankers, who use mathematics to make money &#x2013; or so they hope.

But children, what can they make with mathematics? Not much. They sit in class and they write numbers on pieces of paper. That's not making anything very exciting. So we've tried to find ways that children can use mathematics to make something &#x2013; something interesting, so that the children's relationship to mathematics is more like the engineer's, or the scientist's, or the banker's, or all the important people who use mathematics constructively to construct something.

Well, what sorts of things can they make? Let me give you some examples: children in a school in California, the Gardner Academy, in a project called Project MindStorms, the children made a calendar. And this is the work of a fourth-grade child who programmed the computer to make this shape, thinking of squares and triangles and how to fit them together. And because she had to explain all that to the computer, writing programs in LOGO, she was really using mathematics to make something which she liked, which was even commercially valuable because they sold this calendar and got money to improve their project.

Some of the things are just beautiful. For example, this one. I just got this today and I'm so pleased with it. It was sent to me by a child in Costa Rica, way down in Central America. The child programmed the computer in LOGO, using LOGOWriter and made this beautiful thing. It was photographed and sent to me.

Now if you asked this girl while she was making the picture, "What are you doing?" I don't think she would have said, "I'm programming a computer." I don't think she would have said, "I'm doing mathematics." She would have said, "I'm making a bird, I'm making a picture. I'm going to send it to America." She would have expressed her excitement about what she was doing.

But if you look carefully at how she did it, you see she had to worry about the mathematical description of a curve like a graph. She had to worry about mathematical descriptions of shapes. She was doing mathematics, like a real person &#x2013; a real mathematician, an engineer, a scientist.

And this is what we're trying to do: find ways in which the technology enables children to use knowledge, mathematical knowledge and other knowledge, not just store it in their heads so that twelve years later it's going to be good for them. Nobody can learn well like that; it's a terrible way of learning. We all like to learn so that we can use what we've learned, and that's what we're trying to do with these children.

-   Constructionism vs. Instructionism Part 1: Teaching vs. Learning
-   [Constructionism vs. Instructionism Part 2: LEGO/LEGO Projects](http://www.papert.org/articles/const_inst/const_inst2.html)
-   [Constructionism vs. Instructionism Part 3: Educational Software](http://www.papert.org/articles/const_inst/const_inst3.html)
-   [Constructionism vs. Instructionism Part 4: Advanced Math and LOGO](http://www.papert.org/articles/const_inst/const_inst4.html)
-   [Constructionism vs. Instructionism Part 5: Conclusion](http://www.papert.org/articles/const_inst/const_inst5.html)

