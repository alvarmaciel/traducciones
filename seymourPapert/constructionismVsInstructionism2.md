# Constructionism vs. Instructionism

In the 1980s Seymour Papert delivered the following speech by video to a conference of educators in Japan.

Part 2: LEGO/LOGO Projects


Seymour Papert: Well, some other things by connecting the computer to this kind of construction set -- LEGO. Look carefully under this. You'll see how the gears are carefully designed so that the horses go up and down as the merry-go-round turns. So, somebody was thinking about technology and how to use it -- was thinking about gears, about the relation of small gears and big gears -- but all this was part of making something which was meaningful to the learner.

So, in a way, the computer becomes invisible. The computer becomes just an instrument. I said if you asked that child making the picture, "What are you doing?" she would have said, "Making a picture, making a bird." It's interesting to compare this -- imagine going to a poet and saying, "What are you doing?" You'd be very surprised if the poet said, "I'm using a pencil". The poet would have said, "I'm writing a poem," or, maybe, "Just leave me alone, I'm busy." Of course the poet was using a pencil, but that's not worth mentioning, and the same should be true of computers.

We'd like the computer to become an invisible part of things that learners do. We'd like mathematics to become an invisible part of things that people do, and then only later, when it's been intuitively understood, when it's part of your unconscious mind, then it's time to be formal, and have formal classes, and teach mathematics as an abstract formal subject. Meantime, we should relate it to everything in the world -- to useful things, and to beautiful things.

And let's look at one other of these constructs that were built out of LEGO controlled by LOGO. That one doesn't do anything except be beautiful, but it involves the same kind of principles of programming and using gears, and rotation. And so, the same principles of knowledge can be used in many different ways which match the interests and the desires, and the personality, and the style, of the individual children.

One of the things that's wrong with school, I said, was that what you learn there, you can't really use. Another thing that's wrong with school is that there's one way to do it. And that doesn't happen in the real world either. In the real world, there are many ways to do things, and this is how creativity develops. This is how people make exciting new discoveries -- because they try many different ways to get the results they're looking for. And here, in all the examples I've shown you, we're going to look at some more more closely, children are using knowledge about computers and about mathematics in personal ways to do personal projects. Each one doing something different, but through these different activities, learning the same sort of knowledge.

Does that seem strange? In school we say there's a curriculum: everybody must do the same curriculum or else how can they learn the same thing? To see what nonsense that idea is, think of a baby. All babies -- well, all Japanese babies I suppose, are going to learn to speak Japanese. But we don't think they all ought to say the same things to their mother.

Each one says something different. They just live their lives in different homes, and have different toys, and they have different relationships, each one saying what comes from the heart -- what they feel, what they think. But they all learn Japanese because they're using the same language. And they learn it well. But at school, we try to systemize and make everybody do the same thing. I just can't understand why, except maybe it was because we didn't have the technological possibility to give children this wonderful variety of things to do.

Now I'd like to show you some examples of children working in schools, using computers, programming with LOGO, but doing very, very different things with it. We are going to see a child who's making something like a video game, a game on the screen. And we'll hear this child talk about whether it's more interesting to play the game or to make the game. And two children will disagree, but what could come out of it was that both are interesting. And it's even more fun playing the game if you made it yourself.

Well, making a game draws on the kind of intellectual passion that we see in children when they're glued to the video screen, playing ready-made video games. I have nothing against games, but if the children could make them, or modify them, I think they would learn ten times as much.

- Boy #1: We're making this game.

- Boy #2: And we made this car.

- Teacher: Is that the car there?

- Boy#2: No, these are our two good cars that we made. And this is the car that tries to crash into them.

- Teacher: Which one? Where is it?

- Boy#2: This guy. Doing, doing, doing, doing….

- Boy#1: Okay, come here and you can see it.

- Boy#3: So what do you like better, making a game or playing it?

- Boy#1: Well, making it makes you feel like you achieved something. Just playing it…

- Boy#3: Well, after you've made it, and playing it, doesn't that make you feel like you've achieved something else? You made a game that works?

- Boy#1: Yeah.

- Boy#3: I mean I could make a game that doesn't work, and I wouldn't feel like I achieved something.

- Boy#1: I know….

- Boy#3: Actually playing the game might be the funnest thing. That's what's bad about games -- if you want to play it, you've got to make it.

- Boy#1: Games are the best thing, it's like you play 'em.

- Teacher: Is it more fun making the game, or playing the game that you make?

- Boy#3: Playing the game that you make.

Seymour Papert: Well, you see, these two children have very different approaches. They both like making the games and playing them, but one's more one way and one's more the other way. And what's great about this situation is that each could follow a personal path and they could discuss it with one another. So they could develop a sense of there being different styles.

When we look at the issue involved here you could talk about consumers and producers of games. These kids are not just consuming a game, they're producing it as well. And in the next example, I'd like to talk about children as consumers and producers in another area, namely educational software. 