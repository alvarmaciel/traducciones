# Construccionismo vs. Instruccionismo

En los años de 1980 Seymour Papert dió la siguiente charla por video para una conferencia de educadores en Japón


## Parte 1: Enseñanza vs. Aprendizaje

[Archivo de video](http://www.papert.org/media/video/articles/const_inst/const_inst1.ram)

Seymour Papert: Hola. Saludos a los educadores Japoneses. Lamento no estar ahí con ustedes, pero afortunadamente vivimos en un siglo de tecnología, y puedo usar esta máquina magica creadora-de-imágenes para enviarles mi voz e imagen, a través de la mitad del mundo. Es realmente asombroso. 

De lo que iba a hablar, si estuviera ahí, es sobre cómo la tecnología puede cambiar la forma en que los niños aprenden matemáticas. Y digo cómo los niños pueden aprender matemáticas de forma diferente, no así, cómo nosotros podemos enseñar matemáticas de forma diferente. Esta es una importante distinción. 

Todo mi trabajo está centrado en colaborar en el aprendizaje de los niños, no solo en la enseñanza. E acuñado una frase para esto: Construccionismo e Instruccionismo son nombres para dos abordajes en la innovación educativa. Instruccionismo es la teoría que dice, "Para tener mejor educación, debemos mejorar la instrucción. Y si vamos a usar computadoras, haremos que las computadoras se encarguen de la instrucción." Y eso nos lleva a toda la idea de enseñanza asistida por computadora.

Bueno, enseñar es importante, pero aprender es mucho más importante. Y el construccionismo significa: "Darle a los niños buenas cosas para hacer, así aprenden haciendo, de mucha mejor manera de la que lo hacían antes." Ahora bien, Creo que las nuevas tecnologías son muy, muy ricas a la hora de proveer cosas para hacer a los chicos, de forma que puedan aprender matemáticas como parte de algo real.

Creo que parte del problema con el aprendizaje de las matemáticas en la escuela es que no es como las matemáticas del mundo real, hay ingenieros que usan las matemáticas para hacer puentes o hacer máquinas. Hay científicos que usan las matemáticas para hacer teorías, para crear explicaciones sobre como funcionan los átomos , y como empezó el universo. Los banqueros, que usan las matemáticas para hacer dinero&#x2013; o eso creen ellos.

Pero en el caso de los niños ¿Qué es lo que pueden hacer con las matemáticas? No mucho. Ellos se sientan en las clases y escriben números en papeles. Eso no implica hacer nada muy emocionante. Así que tratamos de encontrar formas en las que los niños puedan usar las matemáticas para hacer algo&#x2013; cosas interesantes, de manera que la relación de los niños con las matemáticas sea más parecida a la de los ingenieros, o los científicos, o los banqueros, o la de toda esa gente importante que usa las matemáticas constructivamente para construir algo.

Entonces ¿Qué tipo de cosas pueden hacer? Déjenme darles algunos ejemplos: una niña de una escuela de California, la Gardener Academy, en un proyecto llamado MindStorms, la niña hizo un calendario. Y este es un trabajo para de niños de cuarto grado, que programaron la computadora para hacer la forma, pensando en como combinar cuadrados y triángulos para que encajen juntos. Y como tenían que explicar todo esto a una computadora, escribiendo programas en LOGO, ellas usó matemáticas reales para hacer algo que le gustaba, que tuvo incluso valor comercial, porque vendieron estos calendarios para conseguir dinero para mejorar sus proyectos.

Algunas de las cosas que hicieron son simplemente bellas. Por ejemplo esta que me llegó justamente hoy y de la que estoy tan contento. Me lo envió un niño de Costa Rica, de lo profundo de America Central. El niño programó la computadora en LOGO, usando LOGOWriter e hizo esta belleza. La fotografiaron y me la enviaron.

Ahora si le preguntan a esta niñas, mientras hacía el dibujo ¿Qué estás haciendo? no creo que ella responda "estoy programando una computadora" o que ella diga "estoy haciendo matemáticas". Ella seguro diría, "Estoy haciendo un pájaro, estoy haciendo un dibujo, lo voy a mandar a NorteAmérica" Ella hubiera expresado la emoción de lo que estaba haciendo.

Pero si miramos con atención lo que ella hizo, verán que tuvo que ocuparse de la descripción matemática de una curva como gráfico. Se tuvo que ocupar de las descripciones matemáticas de las formas. Ella estuvo haciendo matemáticas, como una persona real &#x2013; una matemática rea, una ingeniera, una científica.

Y esto es lo que intentamos hacer: encontrar formas en las cuales la tecnología permita a los niños a usar el conocimiento,  conocimiento matemático y otros tipos de conocimientos, no solo acumularlo en la cabeza de forma que doce años más tarde serán buenos para ellos. Nadie puede aprender bien de esa forma, es una terrible forma de aprendizaje. Todos queremos aprender de manera que podamos usar eso que aprendimos, y eso es lo que intentamos hacer con estos niños

-   Construccionismo vs. Instruccionismo Parte 1: Enseñanza vs. Aprendizaje
-   [Construccionismo vs. Instruccionismo Parte 2: LEGO/LEGO Projects](http://www.papert.org/articles/const_inst/const_inst2.html)
-   [Construccionismo vs. Instruccionismo Parte 3: Software educativo](http://www.papert.org/articles/const_inst/const_inst3.html)
-   [Construccionismo vs. Instruccionismo Matemática avanzada y LOGO](http://www.papert.org/articles/const_inst/const_inst4.html)
-   [Construccionismo vs. Instruccionismo Parte 5: Conclusión](http://www.papert.org/articles/const_inst/const_inst5.html)

