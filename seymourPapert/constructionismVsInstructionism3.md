Seymour Papert: All over the world, huge industries are churning out so-called educational software for computer-aided instruction to teach children this or that or something. Children can make their own educational software, and by making the software, they learn much more than by using it. Because when you make a piece of software, when you teach something, you have to think about what's really going on, you have to think about the ideas.

And the next example we're going to see is an example at the Hennigan School in Boston, where there's a project called "Children as Software Designers". And in this project, children spend two or three months, three or four hours a week, working on a long project to make a piece of software. And the child we're going to see was making a piece of software to explain fractions to the viewer. It's a piece of software explaining something about fractions, and I'm going to give you some little peeks at how one girl did this.

Narrator: To the right of the screen, at the back of the classroom, sits Ebonique, all by herself. This overweight girl spends most of her time in school in her own socially-isolated cocoon. Self-conscious and insecure, she rarely risks participating in classroom discussions, but when she does, quite often her worst fears are realized.

Teacher: Ebonique?

Ebonique: Why are the numbers on top smaller than, um, the numbers… on the bottom?

Other Children: Not always…

Ebonique: Sometimes…

Teacher: It's something that she's wondering about… sometimes. OK, so why are the numbers… what's you're question again?

Ebonique: Why are the numbers on top smaller than those on the bottom?

Narrator: Outside of the classroom, it's the same sad story. Ebonique is not hanging out with the other kids. With the instructional software design project, we did not expect to transform Ebonique's personality.

Ebonique also experienced genuine intellectual excitement with fractions. Starting the project by creating simple representations and dividing geometrical shapes, she suddenly seemed to develop a personal relationship with fractions. She then felt free to play, and it dawned on her that she could see fractions everywhere. She transferred this insight into an idea for a teaching screen, and began intensive planning in her designer's notebook.

She became obsessed with this design, and it took her many hours to finalize and implement it on the computer. This screen is the product. It reads: "This is a house. Almost every shape is 1/2! I am trying to say, that you use fractions, almost every day of your life. All the parts are fractions! Not just the shaded ones…"

The caterpillar turned into a butterfly. Ebonique came to be known for her good ideas, and enjoyed feeling creative and successful. Other children wanted to see or play with her software and gave her positive responses, "I love it" or "This is fresh," they would say, then ask her to teach them how to do things. "How did you ever make these colors change?" This representation became part of the culture. A few weeks later, Tommy's house appeared, and then Paul's.

Seymour Papert: I think the key point about Ebonique, and the key moment, is the transition when fractions stop being teachers' knowledge and become her knowledge. She appropriates fractions. She relates to them. You see her talking about 2/3 as a hard fraction. You see a screen image, which is a typical teacher textbook representation of a fraction as 2/3 of a pie, a circle divided into sectors. This is somebody else's knowledge. Then, all of a sudden, there's a connection. Ebonique is now thinking about fractions, and she's thinking about her thinking about fractions.

Ebonique: 1/2 is one whole.

Narrator: Ebonique believed that if a shape was divided into halves and 1/2 was shaded, the unshaded part was nothing: it was not a half, it was not a fraction. When she discovered that this was not the case, she worked on ways of teaching this to her software users, telling them, "All the parts are fractions, not just the shaded ones." After the project ended, she felt much more comfortable when talking about fractions, and she overcame many of her misconceptions.

Ebonique:This is a whole and if I want to split it in half, this is all I have to do.

Researcher (Idit Harel): Good. Which one is the half? …Which one is the half?

Ebonique: These two sides are the halves, and if I take this out, it'll just be a whole.

Seymour Papert: We've seen Ebonique do something with computers and fractions. What she was doing with fractions does not look like schoolwork. She was explaining ideas like, "Fractions are everywhere all over the world".

What's that got to do with adding and subtracting and multiplying them? Well, an amazing thing is, it has a lot to do with it. Before this project, Ebonique was at the bottom of her class, almost, in mathematics. She was in the weakest math group. After this project, she was in the top math group, and she stayed in the top math group, not only that year, but the rest of her time at the school.

What was going on? How did she get an improvement in how to add and subtract and multiply fractions from doing this? Well of course she was still going to her school classes. She was still being taught. But because she developed a good relationship with fractions, because she was thinking about fractions, and she thought about it as her knowledge, not teachers' knowledge, she could now listen to what was happening in the class, and she could learn from it.

So this is the amazing result of Constructionist learning: that by doing very simple things, the children improve their ability to learn. Ebonique was learning by making software in which she talked about fractions. And I think that's an important thing. At school, children do not talk about mathematics. She did. The two boys with their game were talking about the product of doing programming. 